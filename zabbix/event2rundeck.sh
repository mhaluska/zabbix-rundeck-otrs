#!/bin/bash

## Requirements (examples for CentOS):
# formail:	procmail.x86_64
# jq:		jq.x86_64
# curl:		curl.x86_64

which formail >/dev/null || exit 69
which jq >/dev/null || exit 69
which curl >/dev/null || exit 69

## Exit codes for postfix mail handling
# 75 --> temporary fail --> retry
# 69 --> unavalable fail --> return to sender
retry=75
fail=69

## Define max retry and delay (in seconds) for ticket & event manipulation in Rundeck job
jobretry=5
delay=30

# Create working dir
workdir=/var/log/zabbix/rdevents
[ ! -d $workdir ] && mkdir -p $workdir					# For storing lock files and possible message output

# Catch the mail
timestamp=$(date +%s%N)
msgfile=$workdir/$timestamp.msg
cat - > $msgfile

# Parse required vars from mail
msgid=$(cat $msgfile | formail -x Message-Id | sed -rn 's/^.*\.([0-9A-Z]{12}).*/\1/p')	# Get message ID matching postfix log
project=$(cat $msgfile | formail -x X-Original-To | sed -rn 's/\s+(.*)@.*/\1/p')	# Get project name based on mailto
subject=$(cat $msgfile | formail -x Subject)						# Get email subject
message=$(cat $msgfile | formail -I "" | base64 --decode)				# Get message and decode base64

# Parse subject: {HOST.NAME}:{TRIGGER.NAME}:{TRIGGER.NSEVERITY}:{EVENT.ID}
hostname=$(echo $subject | cut -f 1 -d ':')				# Parse hostname
trigger=$(echo $subject | cut -f 2 -d ':')				# Parse trigger name
severity=$(echo $subject | cut -f 3 -d ':')				# Parse severity ID
eventid=$(echo $subject | cut -f 4 -d ':')				# Parse original Event ID
# Get trigger status (PROBLEM=0, OK=1)
triggerstat=$(echo $message | grep -q "Trigger status: PROBLEM"; echo $?)	# Set OK or PROBLEM events

mv -f $msgfile $workdir/$eventid.$msgid.msg

## Required variables
rdurl="http://127.0.0.1:4440"						# Rundeck URL
rdapi=23								# Rundeck API version
rdtoken=CHANGEME-TOKEN							# Rundeck auth token
okjob="Internal-EventStatus-OK"						# Rundeck job for OK status events
nojob="Internal-Job-NotFound"						# Rundeck job for no job matching trigger name
jobreg="[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"	# Regex check for Rundeck job ID search
# Update num of retry for job start, for postfix redelivery time tune: queue_run_delay, minimal_backoff_time, maximal_backoff_time
let maxretry=10								# Maximum retry number to submit Rundeck job
timeout=2								# curl GET/POST max time for Rundeck api requests and availability check (in seconds)
lock="$workdir/$eventid"						# Lock file used to not send OK status before PROBLEM to Rundeck
datefmt="date --iso-8601=seconds"					# Date format for logs
logfile="/var/log/zabbix/rundeck.log"					# Log file path

## Functions for checks and execution

check_rd () {
	[ -e $lock.failed ] && echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid already failed, stop processing!" && exit $fail

	let check=1
	[ -e $lock ] && let check=$(cat $lock) || touch $lock

	rdstatus=$(curl -m $timeout -s -I $rdurl/user/login | grep -q "HTTP/1.1 200 OK"; echo $?)

	if [ $rdstatus -ne 0 ]; then
		echo "$($datefmt) - WARN - Processing $triggerstat EventID: $eventid stopped, Rundeck server is probably down or booting! Current retry count: $check/$maxretry"

		[ $check -eq $maxretry ] && echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid failed, max retry count reached, stop processing!" && \
		echo "$message" >> $lock.failed && rm -f $lock && exit $fail

		let check=$check+1
		echo $check > $lock
		exit $retry
	else
		if [ $check -ne 1 ]; then		# Don't log first try
			echo "$($datefmt) - INFO - Processing $triggerstat EventID: $eventid can continue, Rundeck server is up and responding. Current retry count: $check/$maxretry"
		fi
	fi
}

check_lock () {
	[ -e $lock.failed ] && echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid stopped, previous problem transaction failed!" && \
	echo "$message" >> $lock.failed && exit $fail

	let check=1
	[ -e $lock.1 ] && let check=$(cat $lock.1)

	if [ -e $lock ]; then
		echo "$($datefmt) - WARN - Processing $triggerstat EventID: $eventid stopped, problem processing for same EventID is running. Current retry count: $check/$maxretry"

		[ $check -eq $maxretry ] && echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid failed, max retry count reached, stop processing!" && \
		echo "$message" >> $lock.failed && exit $fail

		let check=$check+1
		echo $check > $lock.1
		exit $retry
	else
		if [ $check -gt 1 -a $check -lt $maxretry ]; then	# Give PROBLEM some time to finish execution if retry > 1
			echo "$($datefmt) - INFO - Processing $triggerstat EventID: $eventid delayed, retry > 1. Current retry count: $check/$maxretry"
			let check=$check+1
			echo $check > $lock.1
			exit $retry
		else
			if [ $check -ne 1 ]; then	# Don't log first try
				echo "$($datefmt) - INFO - Processing $triggerstat EventID: $eventid can continue, no lock file found. Current retry count: $check/$maxretry"
				[ -e $lock.1 ] && mv $lock.1 $lock
			fi
		fi

	fi
}

send_escalate () {
	jobid=$(curl -m $timeout -s -X GET "$rdurl/api/$rdapi/project/$project/jobs?authtoken=$rdtoken&format=json&jobExactFilter=$nojob" | jq ".[].id" | tr -d '"')

	if [[ $jobid =~ ^$jobreg$ ]]; then
		runjob=$(curl -m $timeout -s -G -X POST "$rdurl/api/$rdapi/job/$jobid/executions?authtoken=$rdtoken&format=json" \
			--data-urlencode "option.hostname=$hostname" \
			--data-urlencode "option.severity=$severity" \
			--data-urlencode "option.eventid=$eventid" \
			--data-urlencode "option.message=$message" \
			--data-urlencode "option.subject=$subject" \
			--data-urlencode "option.triggerstat=$triggerstat" \
			--data-urlencode "option.maxretry=$jobretry" \
			--data-urlencode "option.delay=$delay")
		execid=$(echo $runjob | jq '.id')

		if [[ $execid =~ ^[0-9]+$ ]]; then
			echo "$($datefmt) - INFO - Processing $triggerstat EventID: $eventid has been successful using job $nojob, execution ID: $execid"
			[ "$triggerstat" = "PROBLEM" ] && echo $execid > $lock.execid
			rm -f $lock
		else
			echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid failed, attaching JSON response and stop processing: $runjob"
			echo "$message" >> $lock.failed
			rm -f $lock
			exit $fail
		fi
	else
		echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid failed, Rundeck job $nojob not found!"
		echo "$message" >> $lock.failed
		rm -f $lock
		exit $fail
	fi
}

send_event () {
	jobname=$1
	jobid=$(curl -m $timeout -s -X GET "$rdurl/api/$rdapi/project/$project/jobs?authtoken=$rdtoken&format=json&jobExactFilter=$jobname" | jq ".[].id" | tr -d '"')

	if [[ $jobid =~ ^$jobreg$ ]]; then
		runjob=$(curl -m $timeout -s -G -X POST "$rdurl/api/$rdapi/job/$jobid/executions?authtoken=$rdtoken&format=json" \
			--data-urlencode "option.hostname=$hostname" \
			--data-urlencode "option.severity=$severity" \
			--data-urlencode "option.eventid=$eventid" \
			--data-urlencode "option.message=$message" \
			--data-urlencode "option.subject=$subject" \
			--data-urlencode "option.triggerstat=$triggerstat" \
			--data-urlencode "option.maxretry=$jobretry" \
			--data-urlencode "option.delay=$delay")
		execid=$(echo $runjob | jq '.id')

		if [[ $execid =~ ^[0-9]+$ ]]; then
			echo "$($datefmt) - INFO - Processing $triggerstat EventID: $eventid successful using job $jobname, execution ID: $execid"
			[ "$triggerstat" = "PROBLEM" ] && echo $execid > $lock.execid
			rm -f $lock
		else
			echo "$($datefmt) - ERROR - Processing $triggerstat EventID: $eventid failed, attaching JSON response and stop processing: $runjob"
			echo "$message" >> $lock.failed
			rm -f $lock
			exit $fail
		fi
	else
		send_escalate
	fi
}

## Main script execution
if [ $triggerstat = 0 ]; then
	triggerstat=PROBLEM
	check_rd >> $logfile			# Check Rundeck availability
	send_event $trigger >> $logfile		# Send PROBLEM event
else
	triggerstat=OK
	check_lock >> $logfile			# Check if there is no PROBLEM event in process
	check_rd >> $logfile			# Check Rundeck availability
	send_event $okjob >> $logfile		# Send OK event
fi

exit 0
