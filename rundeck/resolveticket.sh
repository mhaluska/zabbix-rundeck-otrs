#!/bin/bash

## Get PROBLEM ExecutionID and TicketID from Zabbix ack
## Zabbix server login
token=$(curl -s -X POST -H "Content-Type: application/json-rpc" "$RD_OPTION_ZABBIXURL/api_jsonrpc.php" -d \
"{
    \"jsonrpc\": \"2.0\",
    \"method\": \"user.login\",
    \"params\": {
        \"user\": \"$RD_OPTION_ZABBIXUSR\",
        \"password\": \"$RD_OPTION_ZABBIXPW\"
    },
    \"id\": 1
}" | jq '.result')

## Get Zabbix ack messages
zbxresponse=$(curl -s -X POST -H "Content-Type: application/json-rpc" "$RD_OPTION_ZABBIXURL/api_jsonrpc.php" -d \
"{
    \"jsonrpc\": \"2.0\",
    \"method\": \"event.get\",
    \"params\": {
        \"output\": \"extend\",
        \"eventids\": \"$RD_OPTION_EVENTID\",
        \"select_acknowledges\": \"extend\"
    },
    \"auth\": $token,
    \"id\": 1
}")

## Zabbix server logout
logout=$(curl -s -X POST -H "Content-Type: application/json-rpc" "$RD_OPTION_ZABBIXURL" -d \
"{
    \"jsonrpc\": \"2.0\",
    \"method\": \"user.logout\",
    \"params\": [],
    \"id\": 1,
    \"auth\": $token
}")

## Extract required ack message
datafromack () {
	ackmsg=$(echo $zbxresponse | jq '.result[].acknowledges[] | select(.message | startswith("ExecutionID=")).message' | tr -d '"')

	if [[ $ackmsg =~ ^ExecutionID=[0-9]+,TicketID=[0-9]+$ ]]; then
		# Set variables ExecutionID & TicketID
		ExecutionID=$(echo "$ackmsg" | sed -rn 's/ExecutionID=([0-9]+),TicketID=([0-9]+)/\1/p')
		TicketID=$(echo "$ackmsg" | sed -rn 's/ExecutionID=([0-9]+),TicketID=([0-9]+)/\2/p')
		echo -e "PROBLEM event values from Zabbix: ExecutionID=$ExecutionID, TicketID=$TicketID"
	else
		# Give some retry to rest of script
		let maxretry=$RD_OPTION_MAXRETRY-3
		[ $RD_JOB_RETRYATTEMPT -le $maxretry ] && echo "Get data from Zabbix ack FAILED!" && exit 1
	fi
}

## Extract TicketID from OTRS
ticketfromotrs () {
	TicketID=$(curl -s -X POST "$RD_OPTION_OTRSURL/nph-genericinterface.pl/Webservice/REST/TicketSearch?CustomerUserLogin=$RD_OPTION_OTRSUSR&Password=$RD_OPTION_OTRSPW" -d \
		"{ \"Title\":\"$RD_OPTION_SUBJECT\" }" | jq .TicketID[0] | tr -d '"')
	if [[ $TicketID =~ ^[0-9]+$ ]]; then
		echo "TicketID received from OTRS search: $TicketID"
		# Never close ticket when we got no data from exection
		ticketstatus="open"
		ticketsubject="Automation update ticket"
		message="No info from PROBLEM event execution, so not closing this ticket.\n\n$msgpost"
	else
		echo "No OTRS ticket matched our search, without TicketID is not possible update/resolve ticket!"
		exit 1
	fi
}

## Get PROBLEM event execution state
problemstate () {
	response=$(curl -s -X GET "$RD_OPTION_RDURL/api/$RD_OPTION_RDAPI/execution/$ExecutionID?authtoken=$RD_OPTION_RDTOKEN&format=json")
	execstate=$(echo "$response" | jq '.status' | tr -d '"')
	jobname=$(echo "$response" | jq '.job.name' | tr -d '"')

	if [ "$jobname" = "$RD_OPTION_NOJOB" ]; then
		echo "Previous PROBLEM event execution handled by $RD_OPTION_NOJOB!"
		ticketstatus="open"
		ticketsubject="Automation update ticket"
		message="OK event received, but PROBLEM execution handled by $RD_OPTION_NOJOB, so not closing this ticket.\n\n$msgpost"
	elif [ "$execstate" = "succeeded" ]; then
		echo "Previous PROBLEM event execution OK."
		ticketstatus="closed successful"
		ticketsubject="Automation resolve ticket"
		message="OK event received and PROBLEM execution succeeded, so resolving this ticket.\n\n$msgpost"
	elif [ "$execstate" = "running" ]; then
		# Give some retry to rest of script
		let maxretry=$RD_OPTION_MAXRETRY-2
		[ $RD_JOB_RETRYATTEMPT -le $maxretry ] && echo "Previous PROBLEM event execution RUNNING!" && exit 1
		# Never close ticket when PROBLEM execution is running
		ticketstatus="open"
		ticketsubject="Automation update ticket"
		message="OK event received, but PROBLEM execution is still running, so not closing this ticket.\n\n$msgpost"
	elif [[ $execstate =~ ^(aborted|failed)$ ]]; then
		# Never close ticket when PROBLEM execution is aborted/failed
		ticketstatus="open"
		ticketsubject="Automation update ticket"
		message="OK event received, but PROBLEM execution failed, so not closing this ticket.\n\n$msgpost"
	else
		# Never close ticket when PROBLEM execution state lookup failed
		ticketstatus="open"
		ticketsubject="Automation update ticket"
		message="OK event received, but PROBLEM execution check failed, so not closing this ticket.\n\n$msgpost"
	fi
}

## Update/close ticket
updateticket () {
	response=$(curl -s -X PATCH "$RD_OPTION_OTRSURL/nph-genericinterface.pl/Webservice/REST/Ticket/$TicketID?CustomerUserLogin=$RD_OPTION_OTRSUSR&Password=$RD_OPTION_OTRSPW" -d \
	"{
	\"Ticket\":
		{
		\"State\":\"$ticketstatus\",
		\"CustomerUser\":\"$RD_OPTION_OTRSUSR\"
		},
	\"Article\":
		{
		\"Subject\":\"$ticketsubject\",
		\"Body\":\"$message\",
		\"ContentType\":\"text/plain; charset=utf8\"
		}
	}" | jq '.TicketID' | tr -d '"')

	# Check update status
	if [[ $response =~ ^[0-9]+$ ]]; then
		echo "Ticket update OK."
	else
		echo "Ticket update FAILED!"
		exit 1
	fi
}

### Main script execution
datafromack
## Convert message new lines to \n
msgfix=$(echo "$RD_OPTION_MESSAGE" | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g')
## Add execition ID into message
msgpost="$msgfix\n\nExecution link: $RD_JOB_URL"
[ ! -z $ExecutionID ] && problemstate
[ -z $TicketID ] && ticketfromotrs
updateticket
