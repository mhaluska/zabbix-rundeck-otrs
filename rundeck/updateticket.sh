#!/bin/bash

## Set max retry and delay if not defined
[ -z $RD_OPTION_MAXRETRY ] && RD_OPTION_MAXRETRY=0      # No retry if not defined
[ -z $RD_OPTION_DELAY ] && RD_OPTION_DELAY=30           # Default retry delay 30s
let retry=0

## Check if update message has been passed to script
if [ "$RD_OPTION_UPDATE" = "" ]; then
	RD_OPTION_UPDATE = "Automation finished, but no message update has been provided."
fi

## Update ticket
updateticket () {
	ticketid=$(curl -s -X PATCH "$RD_OPTION_OTRSURL/nph-genericinterface.pl/Webservice/REST/Ticket/$RD_OPTION_TICKETID?CustomerUserLogin=$RD_OPTION_OTRSUSR&Password=$RD_OPTION_OTRSPW" -d \
	"{
	\"Ticket\":
		{
		\"State\":\"open\",
		\"CustomerUser\":\"$RD_OPTION_OTRSUSR\"
		},
	\"Article\":
		{
		\"Subject\":\"Automation update ticket\",
		\"Body\":\"$RD_OPTION_UPDATE\",
		\"ContentType\":\"text/plain; charset=utf8\"
		}
	}" | jq '.TicketID' | tr -d '"')
}

## Check ticket update
while [ $retry -le $RD_OPTION_MAXRETRY ]; do
	updateticket
	if [[ $ticketid =~ ^[0-9]+$ ]]; then
		echo "Update OTRS ticket $ticketid OK: $retry/$RD_OPTION_MAXRETRY."
		break
	else
		[ $retry -eq $RD_OPTION_MAXRETRY ] && echo "Update OTRS ticket FAILED, max retry reached: $retry/$RD_OPTION_MAXRETRY!" && exit 1
		echo "Update OTRS ticket FAILED: $retry/$RD_OPTION_MAXRETRY!"
		let retry=$retry+1
		sleep $RD_OPTION_DELAY
	fi
done

exit 0
